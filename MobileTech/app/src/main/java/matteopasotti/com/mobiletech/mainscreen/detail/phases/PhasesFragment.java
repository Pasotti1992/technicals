package matteopasotti.com.mobiletech.mainscreen.detail.phases;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.mobiletech.R;
import matteopasotti.com.mobiletech.pojo.Phase;

public class PhasesFragment extends Fragment {

    private static final String PHASES = "PHASES";

    @BindView(R.id.rvPhases)
    RecyclerView rvPhases;

    PhasesAdapter phasesAdapter;

    LinearLayoutManager layoutManager;

    public static PhasesFragment newInstance(List<Phase> phases) {

        Bundle args = new Bundle();

        PhasesFragment fragment = new PhasesFragment();
        args.putSerializable(PHASES, (Serializable) phases);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_phases, container, false);
        ButterKnife.bind(this, viewGroup);

        initView();

        return viewGroup;
    }

    private void initView() {
        layoutManager = new LinearLayoutManager(getActivity() , LinearLayoutManager.HORIZONTAL , false);
        rvPhases.setLayoutManager(layoutManager);
        phasesAdapter = new PhasesAdapter((List<Phase>) getArguments().getSerializable(PHASES));
        rvPhases.setAdapter(phasesAdapter);
    }
}
