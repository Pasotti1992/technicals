package matteopasotti.com.mobiletech.data.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import matteopasotti.com.mobiletech.data.module.AppModule;
import matteopasotti.com.mobiletech.data.module.NetModule;
import retrofit2.Retrofit;


@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    Retrofit retrofit();
    Application provideApplication();
}
