package matteopasotti.com.mobiletech.data.module;

import android.support.v7.widget.LinearLayoutManager;

import dagger.Module;
import dagger.Provides;
import matteopasotti.com.mobiletech.mainscreen.home.MainActivity;
import matteopasotti.com.mobiletech.mainscreen.home.MainContract;
import matteopasotti.com.mobiletech.mainscreen.home.ProcedureAdapter;
import matteopasotti.com.mobiletech.util.ActivityScope;

@Module
public class MainModule {

    private final MainContract.View mView;

    private MainActivity activity;

    public MainModule(MainContract.View mView, MainActivity activity) {
        this.mView = mView;
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    MainContract.View provideMainView() { return mView; }

    @Provides
    @ActivityScope
    MainActivity provideMainActivity() {return activity; }

    @Provides
    @ActivityScope
    ProcedureAdapter provideProcedureAdapter(MainActivity activity) {
        return new ProcedureAdapter(activity);
    }

    @Provides
    @ActivityScope
    LinearLayoutManager provideLinearLayoutManager(MainActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
