package matteopasotti.com.mobiletech.mainscreen.home;


import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import matteopasotti.com.mobiletech.api.ProceduresApiInterface;
import matteopasotti.com.mobiletech.mainscreen.home.MainContract;
import matteopasotti.com.mobiletech.pojo.Procedure;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainPresenter implements MainContract.Presenter {

    Retrofit retrofit;

    MainContract.View mView;

    @Inject
    public MainPresenter(Retrofit retrofit, MainContract.View view) {
        this.retrofit = retrofit;
        this.mView = view;
    }

    @Override
    public void getProcedures() {
        mView.showLoading(true);

        retrofit.create(ProceduresApiInterface.class).getProcedures().enqueue(new Callback<List<Procedure>>() {
            @Override
            public void onResponse(Call<List<Procedure>> call, Response<List<Procedure>> response) {

                mView.showLoading(false);

                mView.initProcedureList(response.body());
            }

            @Override
            public void onFailure(Call<List<Procedure>> call, Throwable t) {
                Log.d("MainPresenter", t.toString());
            }
        });
    }
}
