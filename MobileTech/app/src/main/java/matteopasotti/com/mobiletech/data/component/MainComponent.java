package matteopasotti.com.mobiletech.data.component;


import dagger.Component;
import matteopasotti.com.mobiletech.data.module.MainModule;
import matteopasotti.com.mobiletech.mainscreen.home.MainActivity;
import matteopasotti.com.mobiletech.util.ActivityScope;

@Component(dependencies = NetComponent.class, modules = MainModule.class)
@ActivityScope
public interface MainComponent {

    void inject(MainActivity activity);
}
