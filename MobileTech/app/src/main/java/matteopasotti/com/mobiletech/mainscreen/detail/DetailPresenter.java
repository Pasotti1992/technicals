package matteopasotti.com.mobiletech.mainscreen.detail;


import android.util.Log;

import javax.inject.Inject;

import matteopasotti.com.mobiletech.api.ProceduresApiInterface;
import matteopasotti.com.mobiletech.pojo.ProcedureDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailPresenter implements DetailContract.Presenter {

    Retrofit retrofit;

    DetailContract.View view;


    @Inject
    public DetailPresenter(Retrofit retrofit, DetailContract.View view) {
        this.retrofit = retrofit;
        this.view = view;
    }


    @Override
    public void getProcedureDetail(String procedureId) {
        retrofit.create(ProceduresApiInterface.class).getProcedureDetail(procedureId).enqueue(new Callback<ProcedureDetail>() {
            @Override
            public void onResponse(Call<ProcedureDetail> call, Response<ProcedureDetail> response) {
                if(response.body() != null){

                    view.loadView(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProcedureDetail> call, Throwable t) {
                Log.d("DetailPresenter", t.toString());
            }
        });
    }
}
