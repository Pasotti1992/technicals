package matteopasotti.com.mobiletech.api;


import java.util.List;

import matteopasotti.com.mobiletech.pojo.Procedure;
import matteopasotti.com.mobiletech.pojo.ProcedureDetail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProceduresApiInterface {

    @GET("/procedures")
    Call<List<Procedure>> getProcedures();

    @GET("procedure_details/{id}")
    Call<ProcedureDetail> getProcedureDetail(@Path("id") String procedureId);
}
