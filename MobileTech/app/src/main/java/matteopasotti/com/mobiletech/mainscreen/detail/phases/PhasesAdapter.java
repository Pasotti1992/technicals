package matteopasotti.com.mobiletech.mainscreen.detail.phases;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.mobiletech.R;
import matteopasotti.com.mobiletech.pojo.Phase;

public class PhasesAdapter extends RecyclerView.Adapter<PhasesAdapter.PhaseViewHolder> {


    private List<Phase> phases;

    public PhasesAdapter(List<Phase> items) {
        phases = items;
    }

    @Override
    public PhaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.phase_row, parent, false);

        PhaseViewHolder holder = new PhaseViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(PhaseViewHolder holder, int position) {

        Phase phase = this.phases.get(position);

        Uri imageUri = Uri.parse(phase.getIcon());
        Glide.with(holder.itemView.getContext()).load(imageUri)
                .thumbnail(0.5f)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imagePhase);

        holder.namePhase.setText(phase.getName());
    }

    @Override
    public int getItemCount() {
        return phases.size();
    }

    public void initPhases(List<Phase> items) {
        this.phases.clear();
        this.phases.addAll(items);
        notifyDataSetChanged();
    }

    public static class PhaseViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imagePhase)
        ImageView imagePhase;

        @BindView(R.id.namePhase)
        TextView namePhase;

        public PhaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
