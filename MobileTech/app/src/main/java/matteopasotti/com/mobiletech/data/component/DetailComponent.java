package matteopasotti.com.mobiletech.data.component;

import dagger.Component;
import matteopasotti.com.mobiletech.data.module.DetailModule;
import matteopasotti.com.mobiletech.mainscreen.detail.DetailActivity;
import matteopasotti.com.mobiletech.util.ActivityScope;

@Component(dependencies = NetComponent.class, modules = DetailModule.class)
@ActivityScope
public interface DetailComponent {

    void inject(DetailActivity detailActivity);
}
