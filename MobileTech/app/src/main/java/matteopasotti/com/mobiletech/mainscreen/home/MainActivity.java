package matteopasotti.com.mobiletech.mainscreen.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.mobiletech.R;
import matteopasotti.com.mobiletech.common.MyApplication;
import matteopasotti.com.mobiletech.data.component.DaggerMainComponent;
import matteopasotti.com.mobiletech.data.module.MainModule;
import matteopasotti.com.mobiletech.mainscreen.detail.DetailActivity;
import matteopasotti.com.mobiletech.mainscreen.home.MainContract;
import matteopasotti.com.mobiletech.mainscreen.home.MainPresenter;
import matteopasotti.com.mobiletech.mainscreen.home.ProcedureAdapter;
import matteopasotti.com.mobiletech.pojo.Procedure;


public class MainActivity extends AppCompatActivity implements MainContract.View, ProcedureAdapter.onSelectProcedure {

    @BindView(R.id.rvProcedures)
    RecyclerView rvProcedures;

    @BindView(R.id.barLoading)
    ProgressBar progressBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    MainPresenter presenter;

    @Inject
    ProcedureAdapter procedureAdapter;

    @Inject
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaggerMainComponent.builder()
                .netComponent(((MyApplication) getApplicationContext()).getNetComponent())
                .mainModule(new MainModule(this, this))
                .build().inject(this);

        initView();

        presenter.getProcedures();
    }

    private void initView() {

        setSupportActionBar(toolbar);

        rvProcedures.setLayoutManager(layoutManager);
        rvProcedures.setAdapter(procedureAdapter);
        procedureAdapter.setOnSelectProcedure(this);
    }

    @Override
    public void showLoading(boolean loading) {
        rvProcedures.setVisibility(loading ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initProcedureList(List<Procedure> items) {
        procedureAdapter.initProcedures(items);
    }

    @Override
    public void onSelected(Procedure procedure) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.PROCEDURE_ID, procedure.getId());
        startActivity(intent);
    }
}
