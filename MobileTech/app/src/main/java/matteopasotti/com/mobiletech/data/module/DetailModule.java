package matteopasotti.com.mobiletech.data.module;

import dagger.Module;
import dagger.Provides;
import matteopasotti.com.mobiletech.mainscreen.detail.DetailContract;
import matteopasotti.com.mobiletech.util.ActivityScope;


@Module
public class DetailModule {

    private final DetailContract.View mView;

    public DetailModule(DetailContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @ActivityScope
    DetailContract.View provideDetailView( ) {return mView; }
}
