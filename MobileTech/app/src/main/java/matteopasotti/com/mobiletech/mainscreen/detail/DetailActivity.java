package matteopasotti.com.mobiletech.mainscreen.detail;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.mobiletech.R;
import matteopasotti.com.mobiletech.common.MyApplication;
import matteopasotti.com.mobiletech.data.component.DaggerDetailComponent;
import matteopasotti.com.mobiletech.data.module.DetailModule;
import matteopasotti.com.mobiletech.mainscreen.detail.phases.PhasesFragment;
import matteopasotti.com.mobiletech.pojo.ProcedureDetail;

public class DetailActivity extends AppCompatActivity implements DetailContract.View {

    public static final String PROCEDURE_ID = "PROCEDURE_ID";

    private String procedureId;

    @BindView(R.id.imageProcedureDetail)
    ImageView imageView;

    @BindView(R.id.toolbarDetail)
    Toolbar toolbar;

    @Inject
    DetailPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        DaggerDetailComponent.builder()
                .netComponent(((MyApplication) getApplicationContext()).getNetComponent())
                .detailModule(new DetailModule(this))
                .build().inject(this);

        initView();

        if(getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().get(PROCEDURE_ID) != null) {
            procedureId = (String) getIntent().getExtras().get(PROCEDURE_ID);
        }

        if(procedureId != null) {
            presenter.getProcedureDetail(procedureId);
        }
    }

    private void initView() {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void loadView(ProcedureDetail detail) {
        Uri imageUri = Uri.parse(detail.getCard());
        Glide.with(this).load(imageUri)
                .thumbnail(0.5f)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

        showFragment(PhasesFragment.newInstance(detail.getPhases()));
    }

    private void showFragment(Fragment fragment) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.replace(R.id.phasesContainer, fragment, fragment.toString());
        ft.commit();
    }
}
