package matteopasotti.com.mobiletech.mainscreen.home;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.mobiletech.R;
import matteopasotti.com.mobiletech.pojo.Procedure;

public class ProcedureAdapter extends RecyclerView.Adapter<ProcedureAdapter.ProcedureHolder> {


    private List<Procedure> procedures;

    private MainActivity activity;

    private onSelectProcedure onSelectProcedure;

    @Inject
    public ProcedureAdapter(MainActivity activity) {

        this.activity = activity;
        procedures = new ArrayList<>();
    }

    @Override
    public ProcedureHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.procedure_row, parent, false);

        final ProcedureHolder holder = new ProcedureHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Procedure procedure = procedures.get(holder.getAdapterPosition());
                if(onSelectProcedure != null) {
                    onSelectProcedure.onSelected(procedure);
                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(ProcedureHolder holder, int position) {

        Procedure procedure = this.procedures.get(position);

        Uri imageUri = Uri.parse(procedure.getIcon());
        Glide.with(activity).load(imageUri)
                .thumbnail(0.5f)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);

        holder.name.setText(procedure.getName());

    }

    @Override
    public int getItemCount() {
        return procedures.size();
    }

    public void initProcedures(List<Procedure> items) {
        this.procedures.clear();
        this.procedures.addAll(items);
        notifyDataSetChanged();
    }

    public interface onSelectProcedure
    {
        void onSelected(Procedure procedure);
    }

    public void setOnSelectProcedure(ProcedureAdapter.onSelectProcedure onSelectProcedure) {
        this.onSelectProcedure = onSelectProcedure;
    }

    public static class ProcedureHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageProcedure)
        ImageView image;

        @BindView(R.id.nameProcedure)
        TextView name;

        public ProcedureHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
