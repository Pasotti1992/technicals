package matteopasotti.com.mobiletech.mainscreen.detail;

import matteopasotti.com.mobiletech.pojo.ProcedureDetail;

/**
 * Created by matteo.pasotti on 20/10/2017.
 */

public interface DetailContract {

    interface View {

        void showMessage(String message);

        void loadView(ProcedureDetail detail);
    }

    interface Presenter {

        void getProcedureDetail(String procedureId);
    }
}
