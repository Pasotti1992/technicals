package matteopasotti.com.mobiletech.mainscreen.home;

import java.util.List;

import matteopasotti.com.mobiletech.pojo.Procedure;

public interface MainContract {

    interface View {

        void showLoading(boolean loading);

        void showMessage(String message);

        void initProcedureList(List<Procedure> items);
    }

    interface Presenter {
        void getProcedures();
    }
}
