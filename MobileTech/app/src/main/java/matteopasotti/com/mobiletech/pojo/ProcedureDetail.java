package matteopasotti.com.mobiletech.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ProcedureDetail implements Serializable {

    private Procedure procedure;

    @SerializedName("card")
    private String card;

    @SerializedName("phases")
    private List<Phase> phases;

    public Procedure getProcedure() {
        return procedure;
    }

    public void setProcedure(Procedure procedure) {
        this.procedure = procedure;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public List<Phase> getPhases() {
        return phases;
    }

    public void setPhases(List<Phase> phases) {
        this.phases = phases;
    }
}
