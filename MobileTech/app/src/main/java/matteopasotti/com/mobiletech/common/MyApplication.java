package matteopasotti.com.mobiletech.common;

import android.app.Application;

import matteopasotti.com.mobiletech.data.component.DaggerNetComponent;
import matteopasotti.com.mobiletech.data.component.NetComponent;
import matteopasotti.com.mobiletech.data.module.AppModule;
import matteopasotti.com.mobiletech.data.module.NetModule;

/**
 * Created by matteo.pasotti on 20/10/2017.
 */

public class MyApplication extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();
    }

    public NetComponent getNetComponent() { return netComponent; }
}
